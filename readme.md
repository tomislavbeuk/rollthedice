Simple "roll the dice" game created using html, css and javascript. The HTML and CSS for this project were created and provided by Jonas Schmedtmann

GAME RULES:
    The game has 2 players, playing in rounds
    In each turn, player rolls a dice as many times as he wants. Each result get added to his ROUND score
    If the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
    The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
    The first player to reach 100 points on GLOBAL score wins the game
