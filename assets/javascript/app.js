
const ROLLTHEDICE = {
    gamePlaying: false,
    diceVal: 0,
    scores: [0,0],
    roundScore: 0,
    activePlayer: 0,
    diceImg: document.querySelector('.dice'),
    diceRollBtn: document.querySelector('.btn-roll'),
    diceHoldBtn: document.querySelector('.btn-hold'),
    newGameBtn:  document.querySelector('.btn-new'),
    initGame: function() {
        let me = this;
        me.activePlayer = 0;
        me.roundScore = 0;
        me.scores = [0,0];
        me.gamePlaying = true;
    
        // Reset values to 0 at the beginning of game
        me.diceImg.style.display = 'none';
        document.querySelector('#score-0').textContent = '0';
        document.querySelector('#score-1').textContent = '0';
        document.querySelector('#current-0').textContent = '0';
        document.querySelector('#current-1').textContent = '0';
        document.querySelector('#name-0').textContent = 'Player 1';
        document.querySelector('#name-1').textContent = 'Player 2';
        document.querySelector('.player-0-panel').classList.remove('winner');
        document.querySelector('.player-1-panel').classList.remove('winner');
        document.querySelector('.player-0-panel').classList.remove('active');
        document.querySelector('.player-1-panel').classList.remove('active');
        document.querySelector('.player-0-panel').classList.add('active');
    },
    nextPlayer: function() {
        let me = this;
        (me.activePlayer == 0) ? me.activePlayer = 1 : me.activePlayer = 0; 
        me.roundScore = 0;
        document.querySelector('#current-0').textContent = 0;
        document.querySelector('#current-1').textContent = 0;
        document.querySelector('.player-0-panel').classList.toggle('active');
        document.querySelector('.player-1-panel').classList.toggle('active');
        me.diceImg.style.display = 'none';
    },
    diceRoll: function() {
        let me = this;
        if(me.gamePlaying) {
            // 1.get random number
            me.diceVal = Math.floor(Math.random() * 6) + 1;

            // 2.display the result
            me.diceImg.style.display = 'block';
            me.diceImg.src = `assets/img/dice-${me.diceVal}.png`;

            // update the round score if dice is not 1
            if(me.diceVal != 1) {
                me.roundScore += me.diceVal;
                document.querySelector('#current-' + me.activePlayer).textContent = me.roundScore;
            }
            else {
                // change active player
                (me.activePlayer == 0) ? me.activePlayer = 1 : me.activePlayer = 0;
                me.roundScore = 0;
                document.querySelector('#current-0').textContent = '0';
                document.querySelector('#current-1').textContent = '0';
                document.querySelector('.player-0-panel').classList.toggle('active');
                document.querySelector('.player-1-panel').classList.toggle('active');
                this.diceImg.style.display = 'none';
            }
        }
    },
    holdScore: function() {
        let me = this;
        if(me.gamePlaying) {
            // 1.add round score to total score
            me.scores[me.activePlayer] += me.roundScore;
            document.querySelector('#score-' + me.activePlayer).textContent = me.scores[me.activePlayer];

            // 2.check if player won the game 
            if(me.scores[me.activePlayer] >= 100) {
                me.gamePlaying = false;
                document.querySelector('#name-' + me.activePlayer).textContent = 'WINNER!';
                this.diceImg.style.display = 'none';
                document.querySelector('.player-' + me.activePlayer + '-panel').classList.add('winner');
                document.querySelector('.player-' + me.activePlayer + '-panel').classList.remove('active');
            }
            else {
                // 3. next player's turn
                me.nextPlayer();
            }
        }
    }
};

(function() {
    ROLLTHEDICE.initGame();
    ROLLTHEDICE.diceRollBtn.addEventListener('click', ROLLTHEDICE.diceRoll.bind(ROLLTHEDICE));
    ROLLTHEDICE.diceHoldBtn.addEventListener('click', ROLLTHEDICE.holdScore.bind(ROLLTHEDICE));
    ROLLTHEDICE.newGameBtn.addEventListener('click', ROLLTHEDICE.initGame.bind(ROLLTHEDICE));
})();

